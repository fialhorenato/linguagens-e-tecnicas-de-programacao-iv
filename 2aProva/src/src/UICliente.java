package src;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class UICliente {

	private JTextArea saida;
	private JTextField cliente;
	private JTextField acao;
	private JButton consultarButton;
	private JButton sairButton;
	private Socket s;

	public UICliente() {
		saida = new JTextArea(10, 50);
		cliente = new JTextField(10);
		acao = new JTextField(10);
		consultarButton = new JButton("Consultar");
		sairButton = new JButton("Sair");
	}

	public void launchFrame() {
		JFrame frame = new JFrame("Sala de Chat");

		frame.setLayout(new BorderLayout());

		saida.setEditable(false);
		frame.add(saida, BorderLayout.WEST);

		JPanel p1 = new JPanel();
		p1.add(new JLabel("Cliente: "));
		p1.add(cliente);
		p1.add(new JLabel("A��o: "));
		p1.add(acao);
		frame.add(p1, BorderLayout.SOUTH);

		JPanel p2 = new JPanel();
		p2.add(consultarButton);
		p2.add(sairButton);
		frame.add(p2, BorderLayout.CENTER);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 210);
		frame.setVisible(true);

		sairButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});

		consultarButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				

				if (cliente.getText().isEmpty() || acao.getText().isEmpty() ) {
					saida.setText("Cliente ou acao vazios!");
					
				} else {
					
					try {
						s = new Socket("localhost", 7531);
					} catch (UnknownHostException x) {
						saida
								.setText("Nao foi possivel fazer a conexao com o servidor!");
					} catch (IOException x) {
						saida
								.setText("Nao foi possivel fazer a conexao com o servidor!");
					}
					
					
					try {
						DataInputStream in = new DataInputStream(s.getInputStream());
						DataOutputStream out = new DataOutputStream(s.getOutputStream());
						String formatada = cliente.getText() + "/" + acao.getText();
						out.writeUTF(formatada);						
						String str = in.readUTF();
						saida.setText(str);						
					} catch (IOException e1) {
						saida
						.setText("Nao foi possivel fazer a conexao com o servidor!");
					}
					
				}

			}
		});
	}

	public static void main(String[] args) {
		UICliente c = new UICliente();
		c.launchFrame();
	}
}
