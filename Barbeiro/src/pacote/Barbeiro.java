package pacote;

public class Barbeiro extends Thread {
	Salao salao;
	
	
	public Barbeiro(Salao salao) {
		this.salao = salao;
	}


	@Override
	public void run() {
		salao.cortarCabelo();
	}

}
