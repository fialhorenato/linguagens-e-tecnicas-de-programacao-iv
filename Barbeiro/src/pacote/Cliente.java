package pacote;

public class Cliente extends Thread {
	Salao salao;

	public Cliente(Salao salao) {
		this.salao = salao;
	}

	@Override
	public void run() {
		salao.ClienteFila();
	}

}
