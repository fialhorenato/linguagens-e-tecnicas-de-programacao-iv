package pacote;

public class Principal {
	public static void main(String[] args) {
		Salao salao = new Salao(10);
		new Barbeiro(salao).start();
		
		for (int i = 0; i < 5; i++) {
			new Cliente(salao).start();
		}
	}
}
