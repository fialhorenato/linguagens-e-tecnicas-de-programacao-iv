package pacote;

public class Salao {

	private int cont;
	private int tamanho;

	public Salao(int tamanho) {
		this.cont = 0;
		this.tamanho = tamanho;

	}
	
	private int FilaCheia() {
		if (cont == tamanho) {
			return 1;
		} else {
			return 0;
		}
	}

	public synchronized void cortarCabelo() {
		System.out.println("Barbeiro dormindo!");
		
		while (cont == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		
		cont--;
		notifyAll();
		System.out.println("Barbeiro cortou o cabelo");
		
	}
	
	public synchronized void ClienteFila() {
		if (FilaCheia() == 1) {
			System.out.println("Cliente vai embora!");
		}
		
		while (FilaCheia() == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
			
			cont++;
			System.out.println("Cliente foi pra fila!");
		}
	}
}