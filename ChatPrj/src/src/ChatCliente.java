package src;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ChatCliente {

  private final String DEFAULT_SERVER_IP = "172.16.4.41";
  private final int DEFAULT_SERVER_PORT = 5432;

  private String serverIP = DEFAULT_SERVER_IP;
  private int serverPort = DEFAULT_SERVER_PORT;

  private Socket connection = null;
  private BufferedReader serverIn = null;
  private PrintStream serverOut = null;

  private JTextArea saida;
  private JTextField entrada;
  private JButton enviarButton;
  private JButton sairButton;
  private JComboBox nomes;
  private JMenuItem sobreMenuItem;
  private JMenuItem sairMenuItem;
  private JDialog sobreDialog;

  public ChatCliente() {
    saida = new JTextArea(10,50);
    entrada = new JTextField(50);
    enviarButton = new JButton("Enviar");
    sairButton = new JButton("Sair");
    String[] nome = {"Erix Bigodinho", "Usu 2", "Usu 3"};
    nomes = new JComboBox(nome);
    sobreMenuItem = new JMenuItem("Sobre");
    sairMenuItem = new JMenuItem("Sair");

    String serverIP = System.getProperty("serverip");
    if(serverIP != null) this.serverIP = serverIP;
    String serverPort = System.getProperty("serverport");
    if(serverPort != null) this.serverPort = Integer.parseInt(serverPort);
  }

  public void launchFrame() {
    JFrame frame = new JFrame("Sala de Chat");

    frame.setLayout(new BorderLayout());
    
    JScrollPane scroll = new JScrollPane(saida);

    frame.add(scroll,BorderLayout.WEST);
    frame.add(entrada,BorderLayout.SOUTH);

    JPanel p1 = new JPanel(); 
    p1.add(enviarButton);
    p1.add(sairButton);
    p1.add(nomes);
    frame.add(p1,BorderLayout.CENTER);

    JMenuBar mb = new JMenuBar();
    JMenu file = new JMenu("Arquivo");
    file.add(sairMenuItem);
    JMenu help = new JMenu("Ajuda");
    help.add(sobreMenuItem);
    mb.add(file);
    mb.add(help);
    frame.setJMenuBar(mb);

    sobreDialog = new SobreDialog(frame,"Sobre",true);

    enviarButton.addActionListener(new EnviarHandler());
    entrada.addActionListener(new InputHandler());
    sairButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
    	  System.exit(0);
      }
    });
    sairMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
    	  System.exit(0);
      }
    });
    sobreMenuItem.addActionListener(new SobreHandler());

    doConnect();

    frame.setSize(630,210);
    frame.setVisible(true);
  }

  private void doConnect() {
    try {
      connection = new Socket(serverIP,serverPort);
      serverIn = 
        new BufferedReader(new InputStreamReader(connection.getInputStream()));
      serverOut = new PrintStream(connection.getOutputStream());
      Thread t = new Thread(new RemoteReader());
      t.start();
    }
    catch(Exception e) {
      System.out.println("Conex�o com o servidor falhou!");
      e.printStackTrace();
    }
  }
    
  private void copyText() {
    String text = entrada.getText();
    serverOut.println(nomes.getSelectedItem()+": "+text);
    entrada.setText("");
  }

  private class EnviarHandler implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      copyText();
    }
  }

  private class InputHandler implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      copyText(); 
    }
  }

  private class SobreHandler implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      sobreDialog.setVisible(true);
    }
  }

  private class SobreDialog extends JDialog implements ActionListener  {
	private static final long serialVersionUID = 1L;

	public SobreDialog(Frame parent, String title, boolean modal) {
      super(parent,title,modal);
      add(new JLabel("Sala de Chat v1.0"),BorderLayout.NORTH);
      JButton b = new JButton("OK");
      add(b,BorderLayout.SOUTH);
      pack();
      b.addActionListener(this);
    }
 
   public void actionPerformed(ActionEvent e) {
     setVisible(false);
   }
  }

  private class RemoteReader implements Runnable {
    private boolean keepListening = true;

    public void run() {
      while(keepListening == true) {
        try {
          String nextLine = serverIn.readLine();
          saida.append(nextLine+"\n");
        }
        catch(Exception e) {
          keepListening = false;
          System.out.println("Erro na leitura da resposta do servidor.");
          e.printStackTrace();
        }
      }
    }
  }

  public static void main(String[] args) {
    ChatCliente c = new ChatCliente();
    c.launchFrame();
  }
}

