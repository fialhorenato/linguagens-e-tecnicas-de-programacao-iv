package src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class ChatServer implements Runnable {

    public final static int DEFAULT_PORT = 5432;
    public final static int DEFAULT_MAX_BACKLOG = 5;
    public final static int DEFAULT_MAX_CONNECTIONS = 20;
    public final static String DEFAULT_HOST_FILE = "hosts.txt";
    public final static String MAGIC = "Abretesesamo";

    private String magic = MAGIC;

    private int port = DEFAULT_PORT;
    private int backlog = DEFAULT_MAX_BACKLOG;
    private int numConnections = 0;
    private int maxConnections = DEFAULT_MAX_CONNECTIONS;
    private String hostfile = DEFAULT_HOST_FILE;
    private Vector<Connection> connections = null;
    private Hashtable<String, String> hostInfo = new Hashtable<String, String>(15);

    public static void main(String[] args) {
        ChatServer cs = new ChatServer();
        cs.go();
    }

    public void go()
    {
        String portString = System.getProperty("port");
        if(portString != null) port = Integer.parseInt(portString);

        String backlogString = System.getProperty("backlog");
        if(backlogString != null) backlog = Integer.parseInt(backlogString);

        String hostFileString = System.getProperty("hostfile");
        if(hostFileString != null) hostfile = hostFileString;

        String magicString = System.getProperty("magic");
        if(magicString != null) magic = magicString;

        String connections = System.getProperty("connections");
        if(connections != null) maxConnections = Integer.parseInt(connections);
        this.connections = new Vector<Connection>(maxConnections);

        System.out.println("Configuracoes do servidor:\n\tPort="+port+"\n\tMax Backlog="+
                backlog+"\n\tMax Conexoes="+maxConnections+
                "\n\tHost File="+hostfile);

        createHostList();

        Thread t = new Thread(this);
        t.start();
    }

    @SuppressWarnings("unchecked")
	private void createHostList() {
        File hostFile = new File(hostfile);
        try {
            System.out.println("Tentando ler hostfile hosts.txt: ");
            FileInputStream fis = new FileInputStream(hostFile);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String aLine = null;
            while((aLine = br.readLine()) != null)
            {
                int spaceIndex = aLine.indexOf(' ');
                if(spaceIndex == -1) {
                    System.out.println("Linha invalida em hostfile: "+aLine);
                    continue;
                }
                String host = aLine.substring(0,spaceIndex);
                String student = aLine.substring(spaceIndex+1);
                System.out.println("Lido: "+student+"@"+host);
                hostInfo.put(host,student);
            }
        }
        catch(Exception e) { e.printStackTrace(); }
    }          

    public void run()
    {
        ServerSocket serverSocket = null;
        Socket communicationSocket;    

        try
        {
            System.out.println("Tentando iniciar servidor...");
            serverSocket = new ServerSocket(port,backlog);
        } 
        catch (IOException e) 
        { 
            System.out.println("Erro na inicializacao do servidor: nao foi possivel abrir port "+port);
            e.printStackTrace();
            System.exit(-1);
        }
        System.out.println ("Servidor iniciado no port "+port);

        // Roda o loop listen/accept eternamente
        while (true)
        {
            try 
            {
            	// Espera por uma conex�o
                communicationSocket = serverSocket.accept();
                HandleConnection(communicationSocket);
            } 
            catch (Exception e)
            { 
                System.out.println("Impossivel tratar child socket.");
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
	public void HandleConnection(Socket connection)
    {
        synchronized(this)
        {
            while(numConnections == maxConnections)
            {
                try { wait(); } 
                catch(Exception e) { e.printStackTrace(); }
            }
        }
        numConnections++;
        Connection con = new Connection(connection);
        Thread t = new Thread(con);
        t.start();
        connections.addElement(con);
    }

    public synchronized void connectionClosed(Connection connection)
    {
        connections.removeElement(connection);
        numConnections--;
        notify();
    }

    public void sendToAllClients(String message)
    {
        Enumeration<Connection> cons = connections.elements();
        while(cons.hasMoreElements())
        {
            Connection c = cons.nextElement();
            c.sendMessage(message);
        }
    }

    class Connection implements Runnable 
    {

        private Socket communicationSocket = null;
        private OutputStreamWriter out = null;
        private BufferedReader in = null;

        public void run()
        {
            OutputStream socketOutput = null;
            InputStream socketInput = null;

            try
            {
                socketOutput = communicationSocket.getOutputStream();
                out = new OutputStreamWriter(socketOutput);
                socketInput = communicationSocket.getInputStream();
                in = new BufferedReader(new InputStreamReader(socketInput));

                InetAddress address = communicationSocket.getInetAddress();
                String hostname = address.getHostName();

                String welcome = 
                    "Conex�o feita do host: "+hostname+"\nTodos digam oi";
                String student = (hostInfo.get(hostname));
                if(student != null) welcome += " para "+student;
                welcome+="!\n";
                ChatServer.this.sendToAllClients(welcome);
                System.out.println("Conexao feita "+student+"@"+hostname);
                sendMessage("Bem vindo "+student+", a senha � "+magic+".\n");
                String input = null;

                while((input = in.readLine()) != null) {
                    if(input.indexOf(magic) != -1) 
                    {
                        sendMessage("Parab�ns "+student+", voc� enviou a senha!\n");
                        System.out.println(student+" enviou a senha!");
                    }
                    else ChatServer.this.sendToAllClients(input+"\n");
                }
            }
            catch(Exception e) { }
            finally
            {
                try 
                {
                    if(in != null) in.close();
                    if(out != null) out.close();
                    communicationSocket.close();
                } 
                catch(Exception e) { e.printStackTrace(); }
                ChatServer.this.connectionClosed(this);
            }
        }

        public Connection(Socket s)
        {
            communicationSocket = s;
        }       

        public void sendMessage(String message)
        {
            try
            {
                out.write(message);
                out.flush();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }
}

