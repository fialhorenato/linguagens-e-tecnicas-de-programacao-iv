package src;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Cliente {

	private JTextArea saida;
	private JTextField dados;
	private JButton ordenarButton;
	private JButton sairButton;
	private Socket s;

	public Cliente() {
		saida = new JTextArea(10, 50);
		dados = new JTextField(40);
		ordenarButton = new JButton("Ordenar");
		sairButton = new JButton("Sair");
	}

	public void launchFrame() {
		JFrame frame = new JFrame("Cliente de Ordenação");

		frame.setLayout(new BorderLayout());

		saida.setEditable(false);
		frame.add(saida, BorderLayout.WEST);

		JPanel p1 = new JPanel();
		p1.add(new JLabel("Dados: "));
		p1.add(dados);
		frame.add(p1, BorderLayout.SOUTH);

		JPanel p2 = new JPanel();
		p2.add(ordenarButton);
		p2.add(sairButton);
		frame.add(p2, BorderLayout.CENTER);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 210);
		frame.setVisible(true);

		sairButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (s != null) {
					try {						
						ObjectOutputStream out = new ObjectOutputStream(s
								.getOutputStream());
						out.writeObject("Sair");
						s.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				System.exit(0);
			}
		});

		ordenarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean verifica = true;
				String[] nums = dados.getText().split("\\s+");
				int tam = nums.length;
				int[] dadosint = new int[tam];
				int i = 0;
				for (String num : nums) {
					try {
						int dado = Integer.parseInt(num);
						dadosint[i] = dado;
						i++;
					} catch (NumberFormatException x) {
						verifica = false;
						saida
								.setText("Voce digitou algo diferente de um inteiro");
					}
				}
				
				if (dados.getText().isEmpty()) {
					saida.setText("Campo dados vazio!");
				} else if (verifica) {
					try {
						int[] dados;
						s = new Socket("localhost", 5432);
						ObjectInputStream in = new ObjectInputStream(s
								.getInputStream());
						ObjectOutputStream out = new ObjectOutputStream(s
								.getOutputStream());
						out.writeObject(dadosint);

						while (true) {
							dados = (int[]) in.readObject();
							for (int dado : dados) {
								saida.setText(saida.getText() + "\t"
										+ String.valueOf(dado));
							}
						}

					} catch (UnknownHostException e1) {
						saida.setText("Nao foi possivel conectar");
					} catch (IOException e1) {
						saida.setText("Nao foi possivel conectar");
					} catch (ClassNotFoundException e1) {
						saida.setText("Nao foi possivel serializar!");
					}
				}
			}

		});
	}

	public static void main(String[] args) {
		String teste = "1 2 34 567    89";
		String[] nums = teste.split("\\s+");
		for (String num : nums)
			System.out.println(num);
		System.out.println("Tamanho " + nums.length);
		Cliente c = new Cliente();
		c.launchFrame();
	}
}