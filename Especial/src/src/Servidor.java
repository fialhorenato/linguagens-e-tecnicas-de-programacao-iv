package src;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class Servidor {

	private static final int PORTA = 5432;

	public static void main(String[] args) {
		try {

			ServerSocket s = new ServerSocket(PORTA);

			while (true) {
				System.out.println("Servidor aguardando conexoes");
				Socket ss = s.accept();
				System.out.println(ss.getInetAddress().getHostName());
				System.out.println(ss.getInetAddress().getHostAddress());
				System.out.println(ss.getPort());
				new Conexao(ss).start();
			}

		} catch (IOException e) {
			System.out.println("Nao foi possivel criar o socket servidor!");
		}
	}

}

class Conexao extends Thread {
	private Socket s;

		public Conexao(Socket s) {
		this.s = s;
	}


	@Override
	public void run() {
		try {
			
			while (true) {
				ObjectInputStream in = new ObjectInputStream(s.getInputStream());
				ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
				int[] dados = (int[]) in.readObject();
				for (int dado : dados) {
					System.out.println(dado);
				}
				Arrays.sort(dados);
				out.writeObject(dados);
				String sair = (String) in.readObject();
				if (sair.equalsIgnoreCase("Sair")) {
					s.close();
				}
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
}
