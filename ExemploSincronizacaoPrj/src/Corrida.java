
public class Corrida extends Thread {
	
	private static Compartilhado c;
	private static boolean pronto = false;

	@Override
	public void run() {
		try {
			for(int i = 0; i < 900; i++) {
				if(i % 60 == 0)
					System.out.println();
				System.out.print(".X".charAt(c.dif()));
				Thread.sleep(20);
			}
			System.out.println();
			pronto=true;
		}
		catch (InterruptedException e) {
			System.exit(-1);
		}
	}
	
	public static void main(String[] args) {
		Thread cor = new Corrida();
		c = new Compartilhado();
		
		try {
			cor.start();
			while(!pronto) {
				c.incr();
				Thread.sleep(20);
			}
		}
		catch (InterruptedException e) {
			System.exit(-1);
		}
	}

}
