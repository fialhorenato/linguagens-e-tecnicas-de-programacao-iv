import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class BorderExemplo {
	
	private JButton botao01;
	private JButton botao02;
	private JButton botao03;
	private JButton botao04;
	private JButton botao05;

	private void lancaFrame() {
		JFrame frame = new JFrame("Border Exemplo");
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		
		botao01 = new JButton("Norte");
		botao02 = new JButton("Sul");
		botao03 = new JButton("Leste");
		botao04 = new JButton("Oeste");
		botao05 = new JButton("Centro");
		
		panel.add(botao01,BorderLayout.NORTH);
		panel.add(botao02,BorderLayout.SOUTH);
		panel.add(botao03,BorderLayout.EAST);
		panel.add(botao04,BorderLayout.WEST);
		panel.add(botao05,BorderLayout.CENTER);
		
		frame.add(panel);
		frame.pack();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		new BorderExemplo().lancaFrame();
	}

}
