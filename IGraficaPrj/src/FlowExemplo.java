import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class FlowExemplo {
	
	private JButton botao01;
	private JButton botao02;
	private JButton botao03;

	private void lancaFrame() {
		JFrame frame = new JFrame("Flow Exemplo");
		
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout()); // Desnecess�rio
		
		PrimeiroListener pl = new PrimeiroListener();
		
		botao01 = new JButton("Bot�o 1");
		botao02 = new JButton("Bot�o 2");
		botao03 = new JButton("Bot�o 3");
		botao01.setActionCommand("Comando 1");
		botao01.addActionListener(pl);
		botao01.addActionListener(new OutroListener(botao03));
		botao02.addActionListener(pl);
		
		panel.add(botao01);
		panel.add(botao02);
		panel.add(botao03);
		
		frame.add(panel);
		frame.pack();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		new FlowExemplo().lancaFrame();
	}

}
