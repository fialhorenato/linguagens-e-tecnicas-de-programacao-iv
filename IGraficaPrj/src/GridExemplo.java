import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GridExemplo {
	
	private JButton botao01;
	private JButton botao02;
	private JButton botao03;
	private JButton botao04;
	private JButton botao05;
	private JButton botao06;

	private void lancaFrame() {
		JFrame frame = new JFrame("Grid Exemplo");
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3,2));
		
		botao01 = new JButton("Bot�o 1");
		botao02 = new JButton("Bot�o 2");
		botao03 = new JButton("Bot�o 3");
		botao04 = new JButton("Bot�o 4");
		botao05 = new JButton("Bot�o 5");
		botao06 = new JButton("Bot�o 6");
		
		panel.add(botao01);
		panel.add(botao02);
		panel.add(botao03);
		panel.add(botao04);
		panel.add(botao05);
		panel.add(botao06);
		
		frame.add(panel);
		frame.pack();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		new GridExemplo().lancaFrame();
	}

}
