import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class HelloFrame {
	
	private void lancaFrame() {
		JFrame frame = new JFrame("Hello frame");
		frame.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setSize(100, 100);
		panel.setBackground(Color.BLUE);
		
		frame.add(panel);
		frame.setSize(200, 200);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		new HelloFrame().lancaFrame();
	}

}
