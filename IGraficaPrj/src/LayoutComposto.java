import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class LayoutComposto {
	
	private JButton oeste;
	private JButton centro;
	private JButton arquivo;
	private JButton ajuda;
	
	public LayoutComposto() {
		oeste = new JButton("Oeste");
		centro = new JButton("Centro");
		arquivo = new JButton("Arquivo");
		ajuda = new JButton("Ajuda");
	}
	
	private void lancaFrame() {
		JPanel panel = new JPanel();
		panel.add(arquivo);
		panel.add(ajuda);
		
		JFrame frame = new JFrame("Layout composto");
		frame.add(centro,BorderLayout.CENTER);
		frame.add(oeste,BorderLayout.WEST);
		frame.add(panel,BorderLayout.SOUTH);
		
		frame.pack();		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		new LayoutComposto().lancaFrame();
	}

}
