import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


public class OutroListener implements ActionListener {
	
	private JButton b;
	
	public OutroListener(JButton b) {
		this.b = b;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Outro manipulador de eventos");
		b.setText("Alterado");
	}

}
