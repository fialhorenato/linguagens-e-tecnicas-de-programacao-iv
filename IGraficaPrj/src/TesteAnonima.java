import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class TesteAnonima {

	private JTextField tf;
	private JFrame frame;
	
	public TesteAnonima() {
		tf = new JTextField(30);
		frame = new JFrame("Teste Mouse");
	}
	
	private void lancaFrame() {
		JLabel label = new JLabel("Clique e arraste o mouse");
		
		frame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				tf.setText("O mouse entrou no recinto");
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				tf.setText("O mouse deixou o recinto");
			}
		});
		
		frame.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				tf.setText("Mouse arrastado para (" + e.getX() + "," + e.getY() + ")");
			}
		});
		
		frame.add(label, BorderLayout.NORTH);
		frame.add(tf, BorderLayout.SOUTH);
		
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		new TesteAnonima().lancaFrame();
	}
}
