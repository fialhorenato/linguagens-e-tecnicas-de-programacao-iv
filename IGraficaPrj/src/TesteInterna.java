import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class TesteInterna {

	private JTextField tf;
	private JFrame frame;
	
	public TesteInterna() {
		tf = new JTextField(30);
		frame = new JFrame("Teste Mouse");
	}
	
	private void lancaFrame() {
		JLabel label = new JLabel("Clique e arraste o mouse");
		
		frame.addMouseListener(new MeuMouseListener());
		frame.addMouseMotionListener(new MeuMouseMotionListener());
		
		frame.add(label, BorderLayout.NORTH);
		frame.add(tf, BorderLayout.SOUTH);
		
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		new TesteInterna().lancaFrame();
	}
	
	private class MeuMouseListener extends MouseAdapter {
		
		@Override
		public void mouseEntered(MouseEvent e) {
			tf.setText("O mouse entrou no recinto");
		}
		
		@Override
		public void mouseExited(MouseEvent e) {
			tf.setText("O mouse deixou o recinto");
		}
		
	}
	
	private class MeuMouseMotionListener extends MouseMotionAdapter {
		@Override
		public void mouseDragged(MouseEvent e) {
			tf.setText("Mouse arrastado para (" + e.getX() + "," + e.getY() + ")");
		}
	}

}
