import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class TesteMouse extends MouseAdapter implements MouseMotionListener {
//public class TesteMouse implements MouseListener, MouseMotionListener {
	
	private JTextField tf;
	private JFrame frame;
	
	public TesteMouse() {
		tf = new JTextField(30);
		frame = new JFrame("Teste Mouse");
	}
	
	private void lancaFrame() {
		JLabel label = new JLabel("Clique e arraste o mouse");
		
		frame.add(label, BorderLayout.NORTH);
		frame.add(tf, BorderLayout.SOUTH);
		
		frame.addMouseListener(this);
		frame.addMouseMotionListener(this);
		
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		new TesteMouse().lancaFrame();
	}

//	@Override
//	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {
		tf.setText("O mouse entrou no recinto");
	}

	@Override
	public void mouseExited(MouseEvent e) {
		tf.setText("O mouse deixou o recinto");
	}

//	@Override
//	public void mousePressed(MouseEvent e) {}
//
//	@Override
//	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {
		String t = "Mouse arrastado para (" + e.getX() + "," + e.getY() + ")";
		tf.setText(t);
	}

	@Override
	public void mouseMoved(MouseEvent e) {}

}
