package iopkg;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

public class Copia {
	public static void main(String[] args) {
		try {
			InputStream in = new FileInputStream("entrada.txt");
			OutputStream out = new FileOutputStream("saida.txt");
			
			Reader inn = new FileReader("entrada.txt");
			Writer outo = new FileWriter("saida.txt");
			System.out.println("Arquivo aberto.");

			int c;

			while ((c = in.read()) != -1)
				out.write(c);

			in.close();
			out.close();
			System.out.println("Arquivo copiado");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}
}
