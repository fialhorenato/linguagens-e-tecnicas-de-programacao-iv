package iopkg;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class Dados {

	public static void main(String[] args) {
		double avogadro = 6.02e23;
		
		try {
			OutputStream pout = new FileOutputStream("dados.txt");
			ObjectOutputStream dout = new ObjectOutputStream(pout);
			dout.writeDouble(avogadro);
			dout.writeBoolean(false);
			System.out.println("Numero Gravado");
			dout.close();
			
			ObjectInputStream din = new ObjectInputStream(new FileInputStream("dados.txt"));
			double lido;
			lido = din.readDouble();
			boolean bool = din.readBoolean();
			din.close();
			System.out.println("Boolean: " + bool);
			System.out.println("Lido: "  + lido);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
