package pkg;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddTeste {
	public static void main(String[] args) {
		try {
			InetAddress host = InetAddress.getByName("www.face.fumec.br");
			System.out.println("IP = " + host.getHostAddress());
			System.out.println("HOSTNAME = " + host.getHostName());
			
			for (byte b : host.getAddress()) {
				System.out.print((0x000000FF & (int)b) + "\t");
			}
			
			InetAddress local = InetAddress.getLocalHost();
			System.out.println("\nIP (LOCAL) = " + local.getHostAddress());
			System.out.println("HOSTNAME (LOCAL) = " + local.getHostName());
			for (byte b : local.getAddress()) {
				System.out.print((0x000000FF & (int)b) + "\t");
			}
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
