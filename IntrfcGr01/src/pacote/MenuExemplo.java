package pacote;

import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class MenuExemplo {

	JFrame frame = new JFrame("Menu Exemplo");;
	private JMenuItem mi1;
	private JMenuItem mi2;
	private JMenuItem mi3;
	private JMenuItem mi4;
	private JMenuItem mi5;
	private JMenuItem mi6;

	private JCheckBoxMenuItem cb1;

	public MenuExemplo() {

		mi1 = new JMenuItem("Novo");
		mi2 = new JMenuItem("Salvar");
		mi3 = new JMenuItem("Abrir");
		mi4 = new JMenuItem("Sair");
		mi5 = new JMenuItem("SubItem");
		mi6 = new JCheckBoxMenuItem("Persistente");
	}

	private void lancaFrame() {

		mi1.addActionListener(new NovoListener());
		mi4.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});

		mi6.addItemListener(new PersistenteListener());

		JMenuBar mb = new JMenuBar();
		frame.setJMenuBar(mb);

		JMenu m1 = new JMenu("Arquivo");
		m1.setMnemonic(KeyEvent.VK_A);
		
		JMenu m2 = new JMenu("Editar");
		m2.setMnemonic(KeyEvent.VK_E);
		JMenu m3 = new JMenu("Configura��o");
		m3.setMnemonic(KeyEvent.VK_C);
		
		mi1.setMnemonic(KeyEvent.VK_N);
		mi1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,Event.CTRL_MASK,true));
		mi1.setToolTipText("Ajuda do item 'Novo'");
		mi4.setMnemonic(KeyEvent.VK_S);
		mi4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,Event.CTRL_MASK,true));
		mi6.setMnemonic(KeyEvent.VK_P);

		mb.add(m1);
		mb.add(m2);
		mb.add(m3);

		m1.add(mi1);
		m1.add(mi2);
		m1.add(mi3);
		m1.addSeparator();
		m1.add(mi4);

		JMenu submenu = new JMenu("Submenu");
		m2.add(submenu);
		submenu.add(mi5);
		m3.add(mi6);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		new MenuExemplo().lancaFrame();
	}

	private class NovoListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(frame, "Menu 'Novo' Acionado");
		}

	}

	private class PersistenteListener implements ItemListener {

		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				JOptionPane.showMessageDialog(frame, "Caixa Selecionada",
						"Sele��o:", JOptionPane.OK_OPTION, null);
			} else {
				JOptionPane.showMessageDialog(frame, "Caixa N�o Selecionada",
						"Sele��o:", JOptionPane.OK_OPTION, null);
			}

		}
	}
}
