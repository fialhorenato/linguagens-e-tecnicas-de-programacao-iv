import java.util.Random;


public class Canibal extends Thread {
	
	private final Random r = new Random();
	
	private int id = -1;
	private PanelaMonitor panela = null;
	
	public Canibal (int id, PanelaMonitor panela) {
		this.id = id;
		this.panela = panela;
	}
	
	@Override
	public void run() {
		while (true) {
			System.out.println("Canibal " + id + " faminto");
			panela.pegarPorcaoDaPanela(id);
			System.out.println("Canibal " + id + " comendo por��o");
			try {
				Thread.sleep(r.nextInt(5) * 1000);
			} catch (InterruptedException e) {}
		}
	}

}
