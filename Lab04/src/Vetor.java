import java.util.Vector;

public class Vetor extends Thread {
	int Vetor[];
	int numero;
	int result;
	int direcao;

	public Vetor(int[] Vetor, int numero, int direcao) {
		this.Vetor = Vetor;
		this.numero = numero;
		this.direcao = direcao;
	}

	public Vetor() {
	}

	public int getResult() {
		return result;
	}

	public int getNumero() {
		return numero;
	}

	public void run() {

		if (direcao == 1) {
			for (int i = 0; i < Vetor.length; i++) {
				if (Vetor[i] == numero) {
					result = i;
				}
			}
		}

		if (direcao == 2) {
			for (int i = Vetor.length - 1; i > 0; i--) {
				if (Vetor[i] == numero) {
					result = i;
				}
			}
		}

	}

	public static void main(String[] args) {
		int[] Inteiros = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
				16, 17, 18, 19, 20, 21, 22, 23 };
		Vetor Thread1 = new Vetor(Inteiros, 9, 1);
		Vetor Thread2 = new Vetor(Inteiros, 9, 2);

		Thread1.start();
		Thread2.start();
		if (Thread1.getNumero() == 9 || Thread2.getNumero() == 9) {
			if (Thread2.getNumero() == 9) {
				Thread1.interrupt();
			}
			if (Thread1.getNumero() == 9) {
				Thread2.interrupt();
			}
		}

		if (Thread2.isInterrupted()) {
			System.out.println("Thread 1 venceu!");
		}
		
		if (Thread1.isInterrupted()) {
			System.out.println("Thread 2 venceu!");
		}

	}
}
