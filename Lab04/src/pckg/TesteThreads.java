package pckg;

public class TesteThreads {
	
	
	public static void main(String[] args) {
		PrintMe t1 = new PrintMe();
		PrintMe t2 = new PrintMe();
		PrintMe t3 = new PrintMe();
		
		t1.setName("Renato");
		t2.setName("Renato1");
		t3.setName("Renato2");
		
		t1.start();
		t2.start();
		t3.start();
		
	}
}
