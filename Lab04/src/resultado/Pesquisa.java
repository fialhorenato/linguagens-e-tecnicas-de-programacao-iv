package resultado;

public class Pesquisa extends Thread {

	private int[] vet;
	private int elemento;
	private boolean direcao;
	private int resultado = -1;
	private Pesquisa colega;

	public Pesquisa(int[] vet, int elemento, boolean direcao) {
		this.vet = vet;
		this.elemento = elemento;
		this.direcao = direcao;
	}

	public int getResultado() {
		return resultado;
	}

	public void setColega(Pesquisa colega) {
		this.colega = colega;
	}

	@Override
	public void run() {
		resultado = -1;
		if (direcao) {
			for (int i = 0; i < vet.length; i++) {

				if (Thread.currentThread().isInterrupted())
					break;

				if (vet[i] == elemento) {
					System.out.println("Thread direta encontrou!");
					colega.interrupt();
					resultado = i;
					break;
				}
			}

		} else {

			for (int i = vet.length - 1; i >= 0; i--) {

				if (Thread.currentThread().isInterrupted())
					break;

				if (vet[i] == elemento) {
					System.out.println("Thread reversa encontrou!");
					colega.interrupt();
					resultado = i;
					break;
				}
			}
		}
	}
}
