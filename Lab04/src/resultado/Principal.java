package resultado;

public class Principal {

	public static void main(String[] args) {
		int[] vet = new int[2000];
		for (int i = 0; i < vet.length; i++)
			vet[i] = i;

		int elemento = 10;
		
		Pesquisa PesqDir = new Pesquisa(vet, elemento, true);
		Pesquisa PesqRev = new Pesquisa(vet, elemento, false);
		
		PesqDir.setColega(PesqRev);
		PesqRev.setColega(PesqDir);

		PesqDir.start();
		PesqRev.start();

		try {
			PesqDir.join();
			PesqRev.join();
		} catch (InterruptedException e) {}

		int rd = PesqDir.getResultado();
		int rr = PesqRev.getResultado();

		if (rd != -1)
			System.out.println("Elemento " + elemento + " foi encontrado em "
					+ rd);
		else if (rr != -1) {
			System.out.println("Elemento " + elemento + " foi encontrado em "
					+ rr);
		} else
			System.out.println("Elemento " + elemento + " n�o encontrado");
	}

}
