
public class Pesquisa extends Thread {
	
	private int[] vet;
	private int elemento;
	private boolean direto;
	private Pesquisa colega;
	
	private int resultado = -1;
	
	public Pesquisa(int[] vet, int elemento, boolean direto) {
		this.vet = vet;
		this.elemento = elemento;
		this.direto = direto;
	}
	
	public int getResultado() {
		return resultado;
	}
	
	public void setColega(Pesquisa colega) {
		this.colega = colega;
	}
	
	@Override
	public void run() {
		resultado = -1;
		if(direto) {
			for(int i = 0; i < vet.length; i++) {
				if(Thread.currentThread().isInterrupted())
					break;
				if(vet[i] == elemento) {
					//System.out.println("Thread direta encontrou");
					colega.interrupt();
					resultado = i;
					break;
				}
			}
		}
		else {
			for(int i = vet.length-1; i >= 0; i--) {
				if(Thread.currentThread().isInterrupted())
					break;
				if(vet[i] == elemento) {
					//System.out.println("Thread reversa encontrou");
					colega.interrupt();
					resultado = i;
					break;
				}
			}
		}
	}

}
