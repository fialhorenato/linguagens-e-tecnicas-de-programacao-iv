
public class Principal {

	public static void main(String[] args) {
		
		int[] vet = new int[10000];
		for(int i = 0; i < vet.length; i++)
			vet[i] = i;
		
		int elemento = 5000;
		
		Pesquisa direto = new Pesquisa(vet, elemento, true);
		Pesquisa reverso = new Pesquisa(vet, elemento, false);
		direto.setColega(reverso);
		reverso.setColega(direto);
		
		direto.start();
		reverso.start();
		
		try {
			direto.join();
			reverso.join();
		}
		catch (InterruptedException e) {}
		
		int rd = direto.getResultado();
		int rr = reverso.getResultado();
		
		if(rd != -1)
			System.out.println("Elemento " + elemento + " encontrado em " + rd);
		else
		if(rr != -1)
			System.out.println("Elemento " + elemento + " encontrado em " + rr);
		else
			System.out.println("Elemento " + elemento + " n�o encontrado");
		
		
	}

}
