package src;

public class MergeSort extends Thread {

	private int[] vet;
	private int ini;
	private int fim;
	private int[] res;

	public MergeSort(int vet[], int ini, int fim) {
		this.vet = vet;
		this.ini = ini;
		this.fim = fim;
	}

	public void run() {
	}

	public void getResultado() {
		for (int i = 0; i < res.length; i++) {
			System.out.print(res[i] + " ");
		}
	}

	public void func() {
		res = new int[fim];
		MergeSortR(vet, ini, fim - 1);
	}

	public void MergeSortR(int[] vet, int ini, int fim) {
		if (ini == fim)
			return;
		else {
			int mid = (ini + fim) / 2;
			MergeSortR(res, ini, mid);
			MergeSortR(res, mid + 1, fim);
			merge(res, ini, mid + 1, fim);
		}

	}

	public void Imprime() {
		for (int i = 0; i < vet.length; i++) {
			System.out.print(vet[i] + " ");
		}
	}

	private void merge(int[] workSpace, int lowPtr, int highPtr, int upperBound) {
		int index = 0; 
		int lowerBound = lowPtr;
		int mid = highPtr - 1;
		int n = upperBound - lowerBound + 1; // # of items

		while (lowPtr <= mid && highPtr <= upperBound)
			if (vet[lowPtr] < vet[highPtr])
				workSpace[index++] = vet[lowPtr++];
			else
				workSpace[index++] = vet[highPtr++];

		while (lowPtr <= mid)
			workSpace[index++] = vet[lowPtr++];

		while (highPtr <= upperBound)
			workSpace[index++] = vet[highPtr++];

		for (index = 0; index < n; index++)
			vet[lowerBound + index] = workSpace[index];
	} // end merge()

}
