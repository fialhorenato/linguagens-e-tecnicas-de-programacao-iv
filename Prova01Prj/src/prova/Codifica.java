package prova;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Codifica {

	private JTextArea resultado = null;
	private JTextField texto = null;
	private JTextField chave = null;
	private JCheckBox des = null;
	private JCheckBox tripleDes = null;
	private JCheckBox exibeTexto = null;
	private JButton codificar = null;
	private JButton sair = null;

	public Codifica() {
		resultado = new JTextArea(10, 40);
		texto = new JTextField(40);
		chave = new JTextField(40);
		des = new JCheckBox();
		tripleDes = new JCheckBox();
		exibeTexto = new JCheckBox();
		codificar = new JButton("Codificar");
		sair = new JButton("Sair");
	}

	private void lancaFrame() {
		JFrame frame = new JFrame("Codificação");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(3, 1));

		JPanel p2 = new JPanel();
		p2.add(new JLabel("Resultado"));
		p2.add(resultado);
		resultado.setEditable(false);
		frame.add(p2, BorderLayout.NORTH);

		JPanel p3 = new JPanel();
		p3.add(new JLabel("Texto"));
		p3.add(texto);

		JPanel p4 = new JPanel();
		p4.add(new JLabel("Chave"));
		p4.add(chave);

		p1.add(p3);
		p1.add(p4);

		frame.add(p1, BorderLayout.CENTER);

		JPanel p5 = new JPanel();
		p5.add(new JLabel("Des"));
		p5.add(des);
		p5.add(new JLabel("Triple Des"));
		p5.add(tripleDes);
		p5.add(new JLabel("Exibe texto?"));
		p5.add(exibeTexto);
		p5.add(codificar);
		p5.add(sair);
		frame.add(p5, BorderLayout.SOUTH);

		frame.pack();
		frame.setVisible(true);
		texto.requestFocus();

		texto.addActionListener(new ResultadoListener());
		
		chave.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				texto.requestFocus();
				
			}
		});

		sair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		texto.addActionListener(new ResultadoListener());

		codificar.addActionListener(new ResultadoListener());
	}

	private class ResultadoListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			resultado.setText(" ");

			if (chave.getText().isEmpty())
				resultado.setText(resultado.getText()
						+ "\nA chave nao esta preenchida");

			if (!des.isSelected() && !tripleDes.isSelected())
				resultado.setText(resultado.getText()
						+ "\nNem o DES nem o Triple DES esta selecionado!");

			if (des.isSelected() || tripleDes.isSelected()) {

				if (exibeTexto.isSelected()) {
					resultado.setText(resultado.getText()
							+ "\nTexto original: " + texto.getText());
				}

				if (!texto.getText().isEmpty() && !chave.getText().isEmpty()) {

					if (des.isSelected() && chave.getText().length() < 8)
						resultado.setText(resultado.getText()
								+ "\n Des nao foi satisfeito!");

					if (des.isSelected() && chave.getText().length() >= 8) {
						try {
							resultado.setText(resultado.getText()
									+ "\nDes:\t"
									+ prova.Criptografia.codifica(texto
											.getText(), chave.getText(),
											prova.Criptografia.DES));
						} catch (IOException e1) {
							e1.printStackTrace();
						} catch (GeneralSecurityException e1) {
							e1.printStackTrace();
						}
					}

					if (tripleDes.isSelected() && chave.getText().length() < 24)
						resultado.setText(resultado.getText()
								+ "\n Triple Des nao foi satisfeito!");

					if (tripleDes.isSelected()
							&& chave.getText().length() >= 24) {
						try {
							resultado.setText(resultado.getText()
									+ "\nTriple Des:\t"
									+ prova.Criptografia.codifica(texto
											.getText(), chave.getText(),
											prova.Criptografia.TRIPLEDES));
						} catch (IOException e1) {
							e1.printStackTrace();
						} catch (GeneralSecurityException e1) {
							e1.printStackTrace();
						}
					}

				}
			}

		}
	}

	public static void main(String[] args) {
		new Codifica().lancaFrame();
	}

}
