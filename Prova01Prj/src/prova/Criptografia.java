package prova;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

public class Criptografia {

   public static final String DES = "DES";
   public static final String TRIPLEDES = "Triple DES";

   private static final String nomeInternoTripleDes = "DESede";

   private static Key gerachave(String chave, String algoritmo)
         throws NoSuchAlgorithmException, InvalidKeyException, 
                InvalidKeySpecException {
      byte[] chv = chave.getBytes();

      KeySpec keySpec = null;
      if (DES.equals(algoritmo))
         keySpec = new DESKeySpec(chv);
      else if (nomeInternoTripleDes.equals(algoritmo))
         keySpec = new DESedeKeySpec(chv);
      else
         throw new NoSuchAlgorithmException(algoritmo);

      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(algoritmo);
      Key key = keyFactory.generateSecret(keySpec);
      return key;
   }

   private static String cifra(String texto, String chave, String algoritmo, int modo)
         throws IOException, GeneralSecurityException {
      String alg = TRIPLEDES.equals(algoritmo) ? nomeInternoTripleDes
            : algoritmo;
      Key key = gerachave(chave, alg);
      Cipher cipher = Cipher.getInstance(alg);
      cipher.init(modo,key);

      int blockSize = cipher.getBlockSize();
      int outputSize = cipher.getOutputSize(blockSize);
      byte[] inBytes = new byte[blockSize];
      byte[] outBytes = new byte[outputSize];

      byte[] inTexto = texto.getBytes();
      InputStream in = new ByteArrayInputStream(inTexto);
      OutputStream out = new ByteArrayOutputStream(inTexto.length);

      int inLength = 0;
      boolean more = true;
      while(more) {
         inLength = in.read(inBytes);
         if(inLength == blockSize) {
            int outLength = cipher.update(inBytes, 0, blockSize, outBytes);
            out.write(outBytes, 0, outLength);
         }
         else more = false;
      }
      if(inLength > 0)
         outBytes = cipher.doFinal(inBytes, 0, inLength);
      else
         outBytes = cipher.doFinal();
      out.write(outBytes);

      return new String(out.toString());
   }

   public static String codifica(String texto, String chave, String algoritmo)
         throws IOException, GeneralSecurityException {
      return cifra(texto,chave,algoritmo,Cipher.ENCRYPT_MODE);
   }

   public static String decodifica(String texto, String chave, String algoritmo)
         throws IOException, GeneralSecurityException {
      return cifra(texto,chave,algoritmo,Cipher.DECRYPT_MODE);
   }

}
