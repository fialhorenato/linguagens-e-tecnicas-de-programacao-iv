package src;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor implements Runnable {
	private final static int PORT = 5432;
	

	public void run() {
		ServerSocket Server = null;
		Socket communicationSocket;
		
		try {
			System.out.println("TENTANDO INICIAR O SERVIDOR...");
			Server = new ServerSocket(PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		while (true) {
			try {
				communicationSocket = Server.accept();
				System.out.println("CLIENTE CONECTADO");
				System.out.println(communicationSocket.getInetAddress().getHostName());
				System.out.println(communicationSocket.getInetAddress().getHostAddress());
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}

	}
}
