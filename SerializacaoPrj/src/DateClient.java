import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Date;


public class DateClient {

	public static void main(String[] args) {
		
		try {
			Socket s = new Socket("localhost", 2111);
			
			ObjectInputStream din = new ObjectInputStream(
					s.getInputStream());
			
			Date date = (Date)din.readObject();
			
			System.out.println("Data atual: " + date);
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
