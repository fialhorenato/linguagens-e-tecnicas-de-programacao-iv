import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;


public class DateServer {

	public static void main(String[] args) {
		
		ServerSocket server = null;
		try {
			server = new ServerSocket(2111);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		while(true) {
			try {
				Socket cliente = server.accept();
				
				ObjectOutputStream dout = new ObjectOutputStream(
						cliente.getOutputStream());
				
//				ObjectInputStream din = new ObjectInputStream(
//						cliente.getInputStream());
				
				dout.writeObject(new Date());
				
				cliente.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
