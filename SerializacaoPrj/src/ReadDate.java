import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Date;


public class ReadDate {

	public static void main(String[] args) {
		
		try {
			ObjectInputStream din = new ObjectInputStream(
					new FileInputStream("date.out"));
			
			Date date = (Date)din.readObject();
			
			System.out.println("Data lida: " + date);
			
			din.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
