import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class User implements Serializable {
	
	private static final long serialVersionUID = 2894796468168782419L;

	private String codigo;
//	private transient String senha;
	private String senha;
	private int acesso;
	
	public User(String codigo, String senha, int acesso) {
		super();
		this.codigo = codigo;
		this.senha = senha;
		this.acesso = acesso;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public int getAcesso() {
		return acesso;
	}
	
	@Override
	public String toString() {
		return "C�digo: " + codigo + " senha: " + senha + " acessos: " + acesso; 
	}
	
	private void writeObject(ObjectOutputStream stream) throws IOException {
		senha = Cesar.converte(senha, 3, false);
		stream.defaultWriteObject();
	}
	
	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		senha = Cesar.converte(senha, 3, true);
	}

}
