import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;


public class WriteDate {

	public static void main(String[] args) {
		
		Date date = new Date();
		System.out.println(date);
		
		try {
			ObjectOutputStream dout = new ObjectOutputStream(
					new FileOutputStream("date.out"));
			
			dout.writeObject(date);
			dout.flush();
			
			dout.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
