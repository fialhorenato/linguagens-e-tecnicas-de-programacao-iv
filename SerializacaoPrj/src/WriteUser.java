import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


public class WriteUser {

	public static void main(String[] args) {
		User user = new User("Fl�vio","abcd",3);
		System.out.println(user);
		
		try {
			ObjectOutputStream dout = new ObjectOutputStream(
					new FileOutputStream("user.out"));
			
			dout.writeObject(user);
			dout.flush();
			
			dout.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
