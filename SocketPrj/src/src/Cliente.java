package src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente {
	private static final int PORTA = 4050;
	private static final String hostname = "localhost";

	public static void main(String[] args) {

		try {
			Socket s = new Socket(hostname, PORTA);
			System.out.println("Conectado ao servidor " + s.getInetAddress().getHostName() );
			DataInputStream in = new DataInputStream(s.getInputStream());
			DataOutputStream out = new DataOutputStream(s.getOutputStream());
			
			for (int i = 0; i < 10; i++) {
				
				System.out.println(1);
				out.writeInt(1);				
			}
			
			s.close();
			
		} catch (UnknownHostException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println("ERRO IO = " + e.getMessage());
		}

	}
}
