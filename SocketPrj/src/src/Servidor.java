package src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Servidor {

	private static final int PORTA = 4050;
	
	public Connection criarConexao() {
	        Connection conn = null;
	        try {

	            conn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/", "sa", "");
	        } catch (SQLException ex) {
	           ex.printStackTrace();
	        }
	        return conn;
	  }

	public static void main(String[] args) {
		try {

			ServerSocket s = new ServerSocket(PORTA);

			while (true) {
				System.out.println("Servidor aguardando conexoes");
				Socket ss = s.accept();

				System.out.println(ss.getInetAddress().getHostName());
				System.out.println(ss.getInetAddress().getHostAddress());
				System.out.println(ss.getPort());
				new Conexao(ss).start();
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}

class Conexao extends Thread {
	private Socket s;

	public Conexao(Socket s) {
		this.s = s;
	}

	@Override
	public void run() {

		try {
			InputStreamReader in = new InputStreamReader(s.getInputStream());
			OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());

			int i = in.readInt();
			while (i != 0) {
				System.out.println("Servidor recebeu" + i);
				out.writeBoolean(i > 0);
				i = in.readInt();
			}
			s.close();
			System.out.println("Cliente encerrou a conexao");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}
}
