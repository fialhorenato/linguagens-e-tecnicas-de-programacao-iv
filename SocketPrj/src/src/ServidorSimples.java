package src;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorSimples {
	private static final int PORTA = 4050;

	public static void main(String[] args) {
		try {
			
			ServerSocket s = new ServerSocket(PORTA);
			
			while (true) {
				System.out.println("Servidor aguardando conexoes");
				Socket ss = s.accept();
				
				System.out.println(ss.getInetAddress().getHostName());
				System.out.println(ss.getInetAddress().getHostAddress());
				System.out.println(ss.getPort());
				
				DataInputStream in = new DataInputStream(ss.getInputStream());
				DataOutputStream out = new DataOutputStream(ss.getOutputStream());
				
				int i = in.readInt();
				while (i != 0) {
					System.out.println("Servidor recebeu" + i);
					out.writeBoolean(i > 0);
					i = in.readInt();
				}
			}
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
