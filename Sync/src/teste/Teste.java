package teste;

public class Teste {

	private int x = 0;
	private int y = 0;
	
	public synchronized void incr() throws InterruptedException {
		x++;
		Thread.sleep(2);
		y++;
	}
	
	public synchronized int dif() {
		return x-y;
	}
	
	public static void main(String[] args) {
		Teste teste = new Teste();
		teste.incr();
	}
}
