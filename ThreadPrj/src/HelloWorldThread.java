public class HelloWorldThread extends Thread {
    public void run() {
    	for(int i = 0; i < 200; i++) {
    		if(Thread.currentThread().isInterrupted()) {
    			System.out.println("Thread secundária interrompida");
    			break;
    		}
    		System.out.println("Hello World Thread (" + i + ")");
    		try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
    			System.out.println("Thread secundária acordou");
    			break;
			}
    	}
    	System.out.println("Thread secundária encerrando");
    }
    public static void main(String[] args) {
    	HelloWorldThread t = new HelloWorldThread();
//    	t.setPriority(Thread.MIN_PRIORITY);
    	System.out.println("Prioridade da thread secundária: " + t.getPriority());
    	System.out.println("Prioridade da thread principal: " + 
    			Thread.currentThread().getPriority());
        t.start();
    	for(int i = 0; i < 100; i++) {
//    		if(i == 200) {
//    			t.interrupt();
//    			System.out.println("Interrompendo a thread secundária");
//    		}
    		System.out.println("Thread principal (" + i + ")");
    		try {
				Thread.sleep(2);
			} catch (InterruptedException e) {}
    	}
    	
    	try {
			t.join();
		} catch (InterruptedException e) {}
    	
    	System.out.println("Thread principal encerrando");
    }
}