import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Cesar {
	
	private JTextArea resultado = null;
	private JTextField texto = null;
	private JTextField deslocamento = null;
	private JCheckBox decifrar = null;
	private JCheckBox exibeTexto = null;
	private JButton codificar = null;
	private JButton sair = null;
	
	public Cesar() {
		resultado = new JTextArea(10,40);
		texto = new JTextField(40);
		deslocamento = new JTextField(35);
		decifrar = new JCheckBox();
		exibeTexto = new JCheckBox();
		codificar = new JButton("Codificar");
		sair = new JButton("Sair");
	}
	
	private void lancaFrame() {
	    JFrame frame = new JFrame("Cifra de C�sar");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	    JPanel p1 = new JPanel();
	    p1.setLayout(new GridLayout(3,1));
	    
	    JPanel p2 = new JPanel();
	    p2.add(new JLabel("Resultado"));
	    p2.add(resultado);
	    resultado.setEditable(false);
	    frame.add(p2,BorderLayout.NORTH);
	    
	    JPanel p3 = new JPanel();
	    p3.add(new JLabel("Texto"));
	    p3.add(texto);
	    
	    JPanel p4 = new JPanel();
	    p4.add(new JLabel("Deslocamento"));
	    p4.add(deslocamento);
	    
	    p1.add(p3);
	    p1.add(p4);
	    
	    frame.add(p1, BorderLayout.CENTER);
	    
	    JPanel p5 = new JPanel();
	    p5.add(new JLabel("Decifrar?"));
	    p5.add(decifrar);
	    p5.add(new JLabel("Exibe texto?"));
	    p5.add(exibeTexto);
	    p5.add(codificar);
	    p5.add(sair);
	    frame.add(p5, BorderLayout.SOUTH);
	    
	    frame.pack();
	    frame.setVisible(true);
	    texto.requestFocus();
	}

	public static void main(String[] args) {
		new Cesar().lancaFrame();
	}

}
